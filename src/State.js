const fs = require('fs');
const config = require('config');

let cachedState = JSON.parse(fs.readFileSync(config.get('stateFile'), 'utf8'));

const get = () => {
	return cachedState;
};

const persist = () => {
	fs.writeFileSync(config.get('stateFile'), JSON.stringify(cachedState), 'utf8');
};

const emails = () => {
	return Object.keys(cachedState.users);
}

const credentials = (email) => {
	return cachedState.users[email].credentials;
};

const setCredentials = (email, credentials) => {
	const user = cachedState.users[email];
	if (user) {
		user.credentials = credentials;
	} else {
		cachedState.users[email] =
			{ credentials: credentials
			, 'reply-message': defaultReply()
			};
	}
	persist();
}

const reply = (email) => {
	return cachedState.users[email]['reply-message'];
};

const setReply = (email, reply) => {
	cachedState.users[email]['reply-message'] = reply;
	persist();
};

const deleteUser = (email) => {
	delete cachedState.users[email];
	persist();
};
		

const defaultReply = () => {
	return cachedState['default-reply'];
};

const setDefaultReply = (reply) => {
	cachedState['default-reply'] = reply;
	persist();
};


module.exports =
	{ get
	, persist
	, emails
	, credentials
	, setCredentials
	, reply
	, setReply
	, deleteUser
	, defaultReply
	, setDefaultReply
	};

