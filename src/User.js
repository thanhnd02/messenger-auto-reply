const messenger = require('facebook-chat-api');


/*
 * Log in using email address and password
 */
const register = (email, password) => {
	return new Promise((resolve, reject) => {
		messenger({ email, password }, (err, api) => {
			if (err) {
				reject(err);
				return;
			}
			console.log('Succesfully logged in');
			resolve(api);
		});
	});
};

/*
 * Log in using session saved in state file.
 */
const login = (credentials) => {
	return new Promise((resolve, reject) => {
		messenger({ appState: credentials }, (err, api) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(api);
		});
	});
};


module.exports =
	{ register
	, login
	};
