#!/usr/bin/env node

const facebook = require('facebook-chat-api');
const fs = require('fs');
const minimist = require('minimist');
const prompt = require('prompt');
const colors = require('colors/safe');

const State = require('./src/State.js');
const User = require('./src/User.js');


const usage = `
SYNOPSIS
	messenger-auto-reply [-h|--help] [-v|--version] [--start] [-l|--list-users]
	                     [--default-reply [<message>]] [-u|--user <email>]
	                     [-r|--reply-message <message>] [--log-out]

OPTIONS
	-h, --help
		Show this help.
	-v, --version
		Show version.
	--start
		Starts auto-replying for all registered users.
	-l, --list-users
		List all registered users.
	--default-reply [<message>]
		Default reply message for new users. If no <message> is given, will print out the current default and exit.
	-u, --user <email>
		Authenticate and register a user with email address <email>. Will prompt for a password.
		This option is required when using user-specific options such as --reply-message or --log-out.
	-r, --reply-message [<message>]
		Reply message for the user passed with -u. If a <message> is given, sets the reply to <message>, otherwise prints the current reply.
	--log-out
		Logs out and forgets the user passed with -u.
`

const showHelp = () => {
	console.log(usage);
};

const showParseError = () => {
	console.log('Could not parse arguments. Pass --help for more info');
};


const updateCredentials = (email, fbApi) => {
	State.setCredentials(email, fbApi.getAppState());
};

const start = async () => {
	const emails = State.emails();
	for (let i = 0; i < emails.length; i++) {
		const email = emails[i];
		const credentials = State.credentials(email);
		try {
			const fbApi = await User.login(credentials);
			updateCredentials(email, fbApi);
			const reply = State.reply(email);
			fbApi.listen((err, message) => {
				if (err) {
					console.error('Something went wrong listening for messages');
					return;
				}
				fbApi.sendMessage(reply, message.threadID);
			});
		} catch (e) {
			console.error(`Something went wrong logging in user ${email}. Maybe the certificate expired? Try reregistering the user.`);
			console.error(e);
		}
	}
};


const register = async (email) => {
	const promptSchema = {
		properties: {
			password: {
				description: colors.cyan(`Please enter the password for ${email}`),
				hidden: true
			}
		}
	};
	const attempt = () => {
		return new Promise((resolve, reject) => {
			prompt.get(promptSchema, async (err, res) => {
				if (err) {
					console.log('interrupt received - exiting...');
					resolve();
					return;
				}
				try {
					const fbApi = await User.register(email, res.password);
					updateCredentials(email, fbApi);
					resolve();
				} catch (e) {
					console.error(e);
					console.log('trying again...');
					await attempt();
					resolve();
				}
			});
		});
	};
	prompt.message = '';
	prompt.delimiter = colors.cyan(":");
	prompt.start();
	await attempt();
};


const listUsers = async () => {
	const emails = State.emails();
	for (let i = 0; i < emails.length; i++) {
		console.log(emails[i]);
	}
};


const handleArgs = async (args) => {
	if (args._.length > 0) {
		showParseError();
		return;
	}
	if (args.length === 1) {
		showHelp();
		return;
	}
	if (args.help || args.h) {
		showHelp();
		return;
	}
	if (args.start) {
		if (args.length > 2 || args.start !== true) {
			showParseError();
			return;
		}
		start();
	}
	if (args.l || args['list-users']) {
		if (args.length > 2 || (args.l && args.l !== true) || (args['list-users'] && args['list-users'] !== true)) {
			showParseError();
			return;
		}
		listUsers();
	}
	if (args['default-reply']) {
		const def = args['default-reply'];
		if (args.length > 2) {
			showParseError();
			return;
		}
		if (def === true) {
			console.log('===');
			console.log(State.defaultReply());
			console.log('===');
			return;
		}
		State.setDefaultReply(def.replace('\\n', '\n').replace('\\t', '\n'));
	}
	if (args.u || args.user) {
		const email = args.u ? args.u : args.user;
		if (email === true) {
			showParseError();
			return;
		}
		if (args.r || args['reply-message']) {
			if (args.length > 3) {
				showParseError();
				return;
			}
			await register(email);
			const reply = args.r ? args.r : args['reply-message'];
			if (reply === true) {
				console.log('===');
				console.log(State.reply(email));
				console.log('===');
				return;
			}
			State.setReply(email, reply.replace('\\n', '\n').replace('\\t', '\n'));
			return;
		}
		if (args['log-out']) {
			if (args.length > 3 || args['log-out'] !== true) {
				showParseError();
				return;
			}
			await register(email);
			State.deleteUser(email);
			console.log('Succesfully logged out!');
			return
		}
		await register(email);
	}
	showHelp();
};

handleArgs(minimist(process.argv.slice(2)));


// vim: set noexpandtab:
